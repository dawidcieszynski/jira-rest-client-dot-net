namespace JIRC.Clients
{
    using System;
    using System.Collections.Generic;

    using JIRC.Domain;

    public class Worklogs
    {
        public IEnumerable<Worklog> WorkLogs { get; set; }
    }

    public class Worklog : AddressableNamedEntity
    {
        public Worklog(Uri self, string name)
            : base(self, name)
        {
        }

        public User Author { get; set; }

        public User UpdateAuthor { get; set; }

        public string Comment { get; set; }

        public Visibility Visibility { get; set; }

        public DateTime Started { get; set; }

        public string TimeSpent { get; set; }

        public int TimeSpentSeconds { get; set; }

        public int Id { get; set; }
    }
}