﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BasicVotes.cs" company="David Bevin">
//   Copyright (c) 2013 David Bevin.
// </copyright>
// // <summary>
//   https://bitbucket.org/dpbevin/jira-rest-client-dot-net
//   Licensed under the BSD 2-Clause License.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace JIRC.Domain
{
    public class TimeTracking
    {
        internal TimeTracking(string originalEstimate, string remainingEstimate, string timeSpent, int originalEstimateSeconds, int remainingEstimateSeconds, int timeSpentSeconds)
        {
            OriginalEstimate = originalEstimate;
            RemainingEstimate = remainingEstimate;
            TimeSpent = timeSpent;
            OriginalEstimateSeconds = originalEstimateSeconds;
            RemainingEstimateSeconds = remainingEstimateSeconds;
            TimeSpentSeconds = timeSpentSeconds;
        }

        public string OriginalEstimate { get; private set; }
        public string RemainingEstimate { get; private set; }
        public string TimeSpent { get; private set; }
        public int OriginalEstimateSeconds { get; private set; }
        public int RemainingEstimateSeconds { get; private set; }
        public int TimeSpentSeconds { get; private set; }
    }
}
